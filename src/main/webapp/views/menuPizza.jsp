<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href='<c:url value="/menu" />'>
                                Menu
                            </a>
                        </li>
                        <li>
                            <a href='<c:url value="/cart" />'>
                                Koszyk
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-success">
            <div class="panel-heading"><h1>Wybierz Pizze</h1></div>
            <div class="panel-body">
                <div class="container" style="font-size: 18px">
                    <div class="col-md-12">
                        <form method="POST" action="<c:url value="/addPizza" /> ">
                            <div class="row">
                                <div class="row list-row form-group">
                                    <div class="col-md-3 text-info text-right">Rodzaj</div>
                                    <div class="col-md-6">
                                        <ul style="list-style: none;">
                                            <c:forEach items="${pizzaTypes}" var="pizzaType" varStatus="loop">
                                                <li>
                                                    <label>
                                                        <input type="radio" name="pizzaTypeIndex" value="${loop.index}"/>
                                                        <strong>${pizzaType.name}</strong>
                                                        <span style="color: crimson">${pizzaType.price}$</span>
                                                    </label>
                                                </li>
                                            </c:forEach>
                                            <c:if test="${pizzaTypeNotSelected}">
                                                <div class="alert alert-warning">
                                                    Wybierz rodzaj pizzy
                                                </div>
                                            </c:if>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row list-row form-group">
                                    <div class="col-md-3 text-info text-right">Rozmiar:</div>
                                    <div class="col-md-6">
                                        <ul style="list-style: none;">
                                            <c:forEach items="${pizzaSizes}" var="size" varStatus="loop">
                                                <li>
                                                    <label>
                                                        <input type="radio" name="pizzaSizeIndex" value="${loop.index}"
                                                            ${loop.index eq 0 ? 'checked' : ""}
                                                        />
                                                            ${size.name}
                                                        <c:if test="${loop.index ne 0}" >
                                                            ( + ${size.extraPrice} $ )
                                                        </c:if>
                                                    </label>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="row list-row form-group">
                                <div class="col-md-3 text-info text-right">Dodatki:</div>
                                <div class="col-md-6">
                                    <ul style="list-style: none;">
                                        <c:forEach items="${pizzaToppings}" var="topping" varStatus="loop">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="pizzaToppings" value="${loop.index}"/>
                                                        ${topping.name} (${topping.extraPrice} $)
                                                </label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            <div class="row list-row">
                                <div class="col-md-3 "></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-lg btn-success">Dodaj do zamówienia</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
