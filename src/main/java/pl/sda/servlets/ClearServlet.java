package pl.sda.servlets;

import pl.sda.helpers.PizzaUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ClearServlet", value = {"/clear"})
public class ClearServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //req.getSession().invalidate();  //metoda invalidate wyczyście całą sesję tzn. wszystkie atrybutu
        req.getSession().setAttribute(PizzaUtils.PIZZA_LIST_ATTR, new ArrayList<>()); // tutaj ustawiamy pustę listę na wybranym atrybucie
        resp.sendRedirect(req.getContextPath() + "/cart"); //wykonujemy przekierowanie na CartServlet
    }
}

