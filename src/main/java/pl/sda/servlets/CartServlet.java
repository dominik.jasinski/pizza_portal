package pl.sda.servlets;

import pl.sda.helpers.PizzaUtils;
import pl.sda.model.Pizza;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CartServlet", value = {"/cart"})
public class CartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/cart.jsp");
        List<Pizza> pizzaList = (List<Pizza>) req.getSession().getAttribute(PizzaUtils.PIZZA_LIST_ATTR);

        if (pizzaList == null) {
            pizzaList = new ArrayList<>();
        }

        double totalPrice = pizzaList.stream().mapToDouble(Pizza::getTotalPrice).sum();
        totalPrice = getWithScale(totalPrice, 2);

        // Ustawiamy atrybuty do których będziemy mieć dostęp na stronie JSP
        req.setAttribute("pizzaList", pizzaList);
        req.setAttribute("totalCount", totalPrice);

        requestDispatcher.forward(req, resp);
    }

    /**
     * Metoda zaokrągla wynik zmienno przecinkowy do wybranej precyzji
     */
    private double getWithScale(double value, int scale) {
        return BigDecimal.valueOf(value)
                .setScale(scale, RoundingMode.HALF_UP)
                .doubleValue();
    }
}

