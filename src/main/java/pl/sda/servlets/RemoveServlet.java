package pl.sda.servlets;

import pl.sda.helpers.PizzaUtils;
import pl.sda.model.Pizza;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "RemoveServlet", value = {"/remove"})
public class RemoveServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Pizza> pizzaList = (List<Pizza>) req.getSession().getAttribute(PizzaUtils.PIZZA_LIST_ATTR);
        int index = -1;

        try {
            index = Integer.valueOf(req.getParameter("index"));
        } catch (NumberFormatException ignored) {

        }

        pizzaList.remove(index); // Wywołanie metody usuwającej element z listy

        resp.sendRedirect(req.getContextPath() + "/cart"); //Ponowne przekierowanie na stronę koszyczka
    }
}

