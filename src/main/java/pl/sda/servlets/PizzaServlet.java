package pl.sda.servlets;

import pl.sda.helpers.PizzaMock;
import pl.sda.helpers.PizzaUtils;
import pl.sda.model.Pizza;
import pl.sda.model.PizzaSize;
import pl.sda.model.PizzaTopping;
import pl.sda.model.PizzaType;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "PizzaServlet", value = {"/menu", "/addPizza"})
public class PizzaServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        renderMenu(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        addPizzaToCart(req);
        renderMenu(req, resp);
    }

    /**
     * Metoda dodająca/aktualizująca atrybut sesji przechowujący listę wybranych pizz
     */
    private void addPizzaToCart(HttpServletRequest request) {
        String pizzaTypeIndexParamValue = request.getParameter("pizzaTypeIndex");
        String pizzaSizeIndexParamValue = request.getParameter("pizzaSizeIndex");
        String[] pizzaToppingIndexParamValues = request.getParameterValues("pizzaToppings");

        if (pizzaTypeIndexParamValue == null) {
            request.setAttribute("pizzaTypeNotSelected", true);
            return;
        } else {
            request.setAttribute("pizzaTypeNotSelected", false);
        }

        int pizzaTypeIndex = Integer.parseInt(pizzaTypeIndexParamValue);
        int pizzaSizeIndex = Integer.parseInt(pizzaSizeIndexParamValue);
        List<Integer> pizzaSizeIndexes = new ArrayList<>();

        if(pizzaToppingIndexParamValues != null) {
            pizzaSizeIndexes= Arrays.stream(pizzaToppingIndexParamValues)
                    .map(stringIndex -> Integer.valueOf(stringIndex))
                    .collect(Collectors.toList());
        }

        updateCart(request, pizzaTypeIndex, pizzaSizeIndex, pizzaSizeIndexes);
    }

    private void renderMenu(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/menuPizza.jsp");
        req.setAttribute("pizzaTypes", PizzaMock.getPizzaTypes());
        req.setAttribute("pizzaSizes", PizzaMock.getPizzaSizes());
        req.setAttribute("pizzaToppings", PizzaMock.getPizzaToppings());

        requestDispatcher.forward(req, resp);
    }

    private void updateCart(HttpServletRequest request, int pizzaTypeIndex, int pizzaSizeIndex, List<Integer> pizzaSizeIndexes) {
        List<Pizza> pizzaList = (List<Pizza>) request.getSession().getAttribute(PizzaUtils.PIZZA_LIST_ATTR);

        if (pizzaList == null) {
            pizzaList = new ArrayList<>();
        }

        Pizza newPizza = createNewPizza(pizzaTypeIndex, pizzaSizeIndex, pizzaSizeIndexes);
        pizzaList.add(newPizza);
        request.getSession().setAttribute(PizzaUtils.PIZZA_LIST_ATTR, pizzaList);
    }

    private Pizza createNewPizza(int pizzaTypeIndex, int pizzaSizeIndex, List<Integer> pizzaSizeIndexes) {
        PizzaType selectedPizzaType = PizzaMock.getPizzaType(pizzaTypeIndex);
        PizzaSize selectedSize = PizzaMock.getPizzaSize(pizzaSizeIndex);
        List<PizzaTopping> selectedToppings = pizzaSizeIndexes.stream().map(
                index -> PizzaMock.getPizzaTopping(index)
        ).collect(Collectors.toList());

        return new Pizza(selectedPizzaType, selectedSize, selectedToppings);
    }
}
