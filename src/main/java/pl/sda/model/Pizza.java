package pl.sda.model;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

    private PizzaType type;
    private PizzaSize size;
    private List<PizzaTopping> toppings = new ArrayList<>();

    public Pizza() {
    }

    public Pizza(PizzaType type, PizzaSize size, List<PizzaTopping> toppings) {
        this.type = type;
        this.size = size;
        this.toppings = toppings;
    }

    public float getTotalPrice() {
        float totalPrice = type.getPrice() + size.getExtraPrice();

        if (toppings.size() > 0) {
            totalPrice += toppings.stream().mapToDouble(PizzaTopping::getExtraPrice).sum();
        }

        return totalPrice;
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public PizzaSize getSize() {
        return size;
    }

    public void setSize(PizzaSize size) {
        this.size = size;
    }

    public List<PizzaTopping> getToppings() {
        return toppings;
    }

    public void setToppings(List<PizzaTopping> toppings) {
        this.toppings = toppings;
    }
}