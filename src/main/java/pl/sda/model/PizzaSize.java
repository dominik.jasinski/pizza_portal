package pl.sda.model;

public class PizzaSize {

    private final String name;
    private final float extraPrice;


    public PizzaSize(String name, float extraPrice) {
        this.name = name;
        this.extraPrice = extraPrice;
    }

    public String getName() {
        return name;
    }

    public float getExtraPrice() {
        return extraPrice;
    }
}