package pl.sda.model;

public class PizzaTopping {

    private final String name;
    private final float extraPrice = 2f;


    public PizzaTopping(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public float getExtraPrice() {
        return extraPrice;
    }
}