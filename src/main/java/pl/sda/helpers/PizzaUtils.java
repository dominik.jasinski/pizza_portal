package pl.sda.helpers;

/**
 * Klasa pomocnicza przechowująca parametry wykorzystywane w wielu klasach
 */
public class PizzaUtils {

    public static final String PIZZA_LIST_ATTR = "pizzaCartList";

    private PizzaUtils() {
    }
}
