package pl.sda.helpers;

import pl.sda.model.PizzaSize;
import pl.sda.model.PizzaTopping;
import pl.sda.model.PizzaType;

import java.util.ArrayList;
import java.util.List;

public class PizzaMock {

    /**
     * Prywatny konstruktor zabezpiecza nam brak możliwości utworzenia instancji klasy PizzaMock
     */
    private PizzaMock() {
    }


    /**
     * Tworzymy mokową listę dostępnych rodzajów pizz
     */
    private static final List<PizzaType> pizzaTypes = new ArrayList<>();

    /**
     * Mockowa lista dostępnych rodzajów rozmiarów pizz
     */
    private static final List<PizzaSize> pizzaSizes = new ArrayList<>();

    /**
     * Mockowa lista dostępnych dodatków do pizzy
     */
    private static final List<PizzaTopping> pizzaToppings = new ArrayList<>();


    /**
     * Ze względu że listy są statyczne zostaną zainicjalizowany tylko raz
     * */
    static {
        pizzaTypes.add(new PizzaType("Wiejska", "kiełbasa, ser, pieczarki", 15));
        pizzaTypes.add(new PizzaType("Vegetarian", "mozarella, pomidory, bazylia, zioła", 11));
        pizzaTypes.add(new PizzaType("Kebab pizza", "kurczak,  cebula czerwona, ogórki kiszone", 14));
        pizzaTypes.add(new PizzaType("Serowa", "mozarella, ser feta, sera żółty", 17));

        pizzaSizes.add(new PizzaSize("Mała", 0));
        pizzaSizes.add(new PizzaSize("Średnia", 3f));
        pizzaSizes.add(new PizzaSize("Duża", 6f));

        pizzaToppings.add(new PizzaTopping("Podwójny ser"));
        pizzaToppings.add(new PizzaTopping("Podwójny ser mozzarella"));
        pizzaToppings.add(new PizzaTopping("Podwójny kurczak"));
        pizzaToppings.add(new PizzaTopping("Podwójne ciasto"));
        pizzaToppings.add(new PizzaTopping("Sos czosnkowy"));
        pizzaToppings.add(new PizzaTopping("Sos pomidorowy"));
    }


    public static List<PizzaType> getPizzaTypes() {
        return pizzaTypes;
    }

    public static PizzaType getPizzaType(int index) {
        PizzaType pizzaType = pizzaTypes.get(index);

        return new PizzaType(pizzaType.getName(), pizzaType.getDescription(), pizzaType.getPrice());
    }

    public static List<PizzaSize> getPizzaSizes() {
        return pizzaSizes;
    }

    public static PizzaSize getPizzaSize(int index) {
        PizzaSize pizzaSize = pizzaSizes.get(index);

        return new PizzaSize(pizzaSize.getName(), pizzaSize.getExtraPrice());
    }

    public static PizzaTopping getPizzaTopping(int index) {
        return pizzaToppings.get(index);
    }

    public static List<PizzaTopping> getPizzaToppings() {
        return pizzaToppings;
    }
}
